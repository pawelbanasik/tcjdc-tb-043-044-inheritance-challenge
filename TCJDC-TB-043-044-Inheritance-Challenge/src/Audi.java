
public class Audi extends Car {

	private String type;

	public Audi(int speed, String name, int size, int weight, int numberOfGears, int gear, int numberOfSeats,
			String type) {
		super(speed, name, size, weight, numberOfGears, gear, numberOfSeats);
		this.type = type;
	}
	
	public void nitro () {
		
		System.out.println("Using nitro");
		
	}

	public String getType() {
		return type;
	}
	
	
	
}

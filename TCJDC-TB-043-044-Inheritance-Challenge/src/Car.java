
public class Car extends Vehicle {

	private int numberOfGears;
	private int gear;
	private int numberOfSeats;
	
	
	
	public Car(int speed, String name, int size, int weight, int numberOfGears, int gear, int numberOfSeats) {
		super(speed, name, size, weight);
		this.numberOfGears = numberOfGears;
		this.gear = gear;
		this.numberOfSeats = numberOfSeats;
	}



	public int getNumberOfGears() {
		return numberOfGears;
	}



	public int getGear() {
		return gear;
	}



	public int getNumberOfSeats() {
		return numberOfSeats;
	}



	public void changingGears (int gear) {
		System.out.println("Car is in " + gear + " gear.");
		
	}
	
	
}
